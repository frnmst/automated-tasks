#!/usr/bin/env python3
#
# monthly_attendance_paper.py
#
# Copyright (C) 2021 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import calendar
import math
import shlex
import sys

import yaml

if __name__ == '__main__':
    configuration_file = shlex.quote(sys.argv[1])
    year = int(shlex.quote(sys.argv[2]))
    month = int(shlex.quote(sys.argv[3]))
    config = yaml.load(open(configuration_file, 'r'), Loader=yaml.SafeLoader)

    days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']
    day_of_week, last_day_of_month = calendar.monthrange(year, month)
    day = 1
    elements = list()
    for i in range(0, last_day_of_month):
        # Select the proper highlighting.
        if config['string']['other']['highlight day'] == days[day_of_week]:
            highlight_start = config['string']['other']['highlight string start']
            highlight_end = config['string']['other']['highlight string end']
        else:
            highlight_start = ' ' * len(config['string']['other']['highlight string start'])
            highlight_end = ' ' * len(config['string']['other']['highlight string end'])

        # Add a leading zero to the day and month when needed
        # so that all the elements have the same length.
        day_id = highlight_start + str(day).zfill(2) + config['string']['separator']['day month'] + str(month).zfill(2) + ' ' + config['days of week'][days[day_of_week]] + highlight_end
        elements.append(day_id)

        day_of_week = (day_of_week + 1) % 7
        day += 1

    #########
    # Print #
    #########
    print(config['information']['header']['title']['value'])
    print(config['information']['header']['employer']['variable'], end='')
    print(': ' + config['information']['header']['employer']['value'])
    print(config['information']['header']['employee']['variable'], end='')
    print(': ' + config['information']['header']['employee']['value'])
    print(config['information']['header']['month and year']['variable'], end='')
    print(': ' + str(month) + ' ' + str(year))
    print()

    total_elements_rounded = math.floor(len(elements) / 2)
    for i in range(0, total_elements_rounded):
        # Strip the newline character which is interpreted by YAML
        # and that is useless as separator for the days.
        print(elements[i] + config['string']['separator']['days'].rstrip('\n') + elements[i + total_elements_rounded], end='')
        print(config['string']['separator']['newline'], end='')

    # Print the last element if the month we are
    # dealing with has an odd number of days.
    if len(elements) / 2 > total_elements_rounded:
        print(config['string']['separator']['odd day'].rstrip('\n') + elements[-1], end='')
        print(config['string']['separator']['newline'], end='')

    print(config['information']['footer']['format']['variable'], end='')
    print(': "' + config['information']['footer']['format']['value'] + '"')

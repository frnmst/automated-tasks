#!/usr/bin/env python3
#
# build_python_packages.py
#
# Copyright (C) 2021 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
r"""build_python_packages.py."""

import contextlib
import os
import pathlib
import shlex
import shutil
import signal
import subprocess
import sys

import fpyutils
import yaml
from appdirs import AppDirs


class InvalidCache(Exception):
    pass


class InvalidConfiguration(Exception):
    pass


def check_keys_type(keys: list, key_type) -> bool:
    """Check that all elements of a list correspond to a specific python type."""
    ok = True

    i = 0
    while ok and i < len(keys):
        if isinstance(keys[i], key_type):
            ok = ok & True
        else:
            ok = ok & False
        i += 1

    return ok


def check_values_type(keys: list, values: dict, level_0_value_type, level_1_value_type, has_level_1_value: bool) -> bool:
    """Check that all elements of a list correspond to a specific python type."""
    ok = True

    i = 0
    while ok and i < len(values):
        if isinstance(values[keys[i]], level_0_value_type):
            j = 0
            while has_level_1_value and ok and j < len(values[keys[i]]):
                if isinstance(values[keys[i]][j], level_1_value_type):
                    ok = ok & True
                else:
                    ok = ok & False
                j += 1
        else:
            ok = ok & False

        i += 1

    return ok


##################################
# Check configuration structure  #
##################################
def check_configuration_structure_ignore(configuration: dict) -> bool:
    ok = True
    if ('submodules' in configuration
       and isinstance(configuration['submodules'], dict)
       and 'ignore' in configuration['submodules']
       and isinstance(configuration['submodules']['ignore'], dict)):

        ignore_keys = list(configuration['submodules']['ignore'].keys())

        ok = ok & check_keys_type(ignore_keys, str)
        ok = ok & check_values_type(ignore_keys, configuration['submodules']['ignore'], list, str, True)

    return ok


def check_configuration_structure_base_directory_override(configuration: dict) -> bool:
    ok = True
    if ('submodules' in configuration
       and isinstance(configuration['submodules'], dict)
       and 'base_directory_override' in configuration['submodules']
       and isinstance(configuration['submodules']['base_directory_override'], dict)):

        base_directory_override_keys = list(configuration['submodules']['base_directory_override'].keys())
        ok = ok & check_keys_type(base_directory_override_keys, str)
        ok = ok & check_values_type(base_directory_override_keys, configuration['submodules']['base_directory_override'], str, None, False)
    else:
        ok = False

    return ok


def check_configuration_structure_checkout(configuration: dict) -> bool:
    ok = True
    if ('submodules' in configuration
       and 'checkout' in configuration['submodules']
       and isinstance(configuration['submodules'], dict)
       and isinstance(configuration['submodules']['checkout'], dict)):

        checkout_keys = list(configuration['submodules']['checkout'].keys())
        ok = ok & check_keys_type(checkout_keys, str)
        ok = ok & check_values_type(checkout_keys, configuration['submodules']['checkout'], list, str, True)
    else:
        ok = False

    return ok


def check_configuration_structure_build(configuration: dict) -> bool:
    ok = True
    if ('submodules' in configuration
       and 'build' in configuration['submodules']
       and isinstance(configuration['submodules'], dict)
       and isinstance(configuration['submodules']['build'], dict)
       and 'pre_commands' in configuration['submodules']['build']
       and 'post_commands' in configuration['submodules']['build']):
        pre = configuration['submodules']['build']['pre_commands']
        post = configuration['submodules']['build']['post_commands']
        pre_commands_block_keys = list(pre.keys())
        ok = ok & check_keys_type(pre_commands_block_keys, str)
        post_commands_block_keys = list(post.keys())
        ok = ok & check_keys_type(post_commands_block_keys, str)

        i = 0
        while ok and i < len(pre):
            pre_section = pre[list(pre.keys())[i]]
            ok = ok & check_values_type(list(pre_section.keys()), pre_section, list, str, True)
            i += 1
        i = 0
        while ok and i < len(post):
            post_section = pre[list(post.keys())[i]]
            ok = ok & check_values_type(list(post_section.keys()), post_section, list, str, True)
            i += 1
    else:
        ok = False

    return ok


def check_configuration_structure(configuration: dict) -> bool:
    ok = True
    if ('repository' in configuration
       and 'submodules' in configuration
       and 'path' in configuration['repository']
       and 'remote' in configuration['repository']
       and 'default branch' in configuration['repository']
       and isinstance(configuration['submodules'], dict)
       and isinstance(configuration['repository']['path'], str)
       and isinstance(configuration['repository']['remote'], str)
       and isinstance(configuration['repository']['default branch'], str)
       and 'ignored_are_successuful' in configuration['submodules']
       and 'mark_failed_as_successful' in configuration['submodules']
       and isinstance(configuration['submodules']['ignored_are_successuful'], bool)
       and isinstance(configuration['submodules']['mark_failed_as_successful'], bool)):
        ok = True
    else:
        ok = False

    ok = ok & check_configuration_structure_ignore(configuration)

    return ok


def elements_are_unique(struct: list) -> bool:
    unique = False
    if len(struct) == len(set(struct)):
        unique = True

    return unique


#########################
# Check cache structure #
#########################
def check_cache_structure(cache: dict) -> bool:
    ok = True
    if not isinstance(cache, dict):
        ok = False

    elements = list(cache.keys())

    ok = ok & check_keys_type(elements, str)

    # Check that tags are unique within the same git repository.
    i = 0
    while ok and i < len(cache):
        if not elements_are_unique(cache[elements[i]]):
            ok = ok & False
        i += 1

    ok = ok & check_values_type(elements, cache, list, str, True)

    return ok


###########
# Generic #
###########
def send_notification(message: str, notify: dict):
    m = notify['gotify']['message'] + '\n' + message
    if notify['gotify']['enabled']:
        fpyutils.notify.send_gotify_message(
            notify['gotify']['url'],
            notify['gotify']['token'], m,
            notify['gotify']['title'],
            notify['gotify']['priority'])
    if notify['email']['enabled']:
        fpyutils.notify.send_email(message,
                                   notify['email']['smtp server'],
                                   notify['email']['port'],
                                   notify['email']['sender'],
                                   notify['email']['user'],
                                   notify['email']['password'],
                                   notify['email']['receiver'],
                                   notify['email']['subject'])


# See
# https://stackoverflow.com/a/13847807
# CC BY-SA 4.0
# spiralman, bryant1410
@contextlib.contextmanager
def pushd(new_dir):
    previous_dir = os.getcwd()
    os.chdir(new_dir)
    try:
        yield
    finally:
        os.chdir(previous_dir)


def build_message(total_tags: int, total_successful_tags: int) -> str:
    message = '\ncurrent successful package git tags: ' + str(total_successful_tags)
    message += '\npackage git tags: ' + str(total_tags)
    if total_tags != 0:
        message += '\ncurrent success rate percent: ' + str((total_successful_tags / total_tags) * 100)
    else:
        message += '\ncurrent success rate percent: 0'
    message += '\n\nnote: tagless repositories are not included in the count'

    return message


def print_information(repository_name: str, git_ref: str, message: str = 'processing'):
    print('\n==========================')
    print(message + ': ' + repository_name + '; git ref: ' + git_ref)
    print('==========================\n')


#########
# Files #
#########
def read_yaml_file(file: str) -> dict:
    data = dict()
    if pathlib.Path(file).is_file():
        data = yaml.load(open(file, 'r'), Loader=yaml.SafeLoader)

    return data


def read_cache_file(file: str) -> dict:
    cache = read_yaml_file(file)
    if not check_cache_structure(cache):
        raise InvalidCache

    return cache


def write_cache(cache: dict, cache_file: str):
    with open(cache_file, 'w') as f:
        f.write(yaml.dump(cache))


#######
# Git #
#######
def git_get_updates(git_executable: str, repository_path: str, repository_remote: str, repository_default_branch: str):
    with pushd(repository_path):
        fpyutils.shell.execute_command_live_output(
            shlex.quote(git_executable)
            + ' pull '
            + repository_remote
            + ' '
            + repository_default_branch
        )
        fpyutils.shell.execute_command_live_output(shlex.quote(git_executable) + ' submodule sync')

        # We might need to add the '--recursive' option for 'git submodule update'
        # to build certain packages. This means that we still depend from external
        # services at build time if we use that option.
        fpyutils.shell.execute_command_live_output(shlex.quote(git_executable) + ' submodule update --init --remote')

        fpyutils.shell.execute_command_live_output(shlex.quote(git_executable) + ' submodule foreach git fetch --tags --force')


def git_remove_untracked_files(git_executable: str):
    fpyutils.shell.execute_command_live_output(shlex.quote(git_executable) + ' checkout -- .')
    fpyutils.shell.execute_command_live_output(shlex.quote(git_executable) + ' clean -d --force')


def git_get_tags(git_executable: str) -> list:
    r"""Get the list of tags of a git repository.

    :returns: s, a list of tags. In case the git repository has no tags, s is an empty list.
    """
    s = subprocess.run([shlex.quote(git_executable), 'tag'], check=True, capture_output=True)
    s = s.stdout.decode('UTF-8').rstrip().split('\n')

    # Avoid an empty element when there are no tags.
    if s == ['']:
        s = list()

    return s


def git_filter_processed_tags(tags: list, cache: dict, software_name: str, successful_tags: list) -> list:
    r"""Given a list of tags filter out the ones already present in cache."""
    # Filter tags not already processed.
    if software_name in cache:
        tags = list(set(tags) - set(cache[software_name]))

        # Tags already in cache must be successful.
        for t in cache[software_name]:
            successful_tags.append(t)

        # Git repositories without tags: remove cache reference if present.
        if tags == cache[software_name] and tags == ['']:
            successful_tags.pop()
            tags = []

    return tags


def git_filter_ignore_tags(tags: list, ignore_objects: dict, skip_tags: bool, software_name: str) -> list:
    r"""Given a list of tags filter out the ones in an ignore list."""
    if skip_tags:
        tags = list(set(tags) - set(ignore_objects[software_name]))

    return tags


def git_get_repository_timestamp(git_executable: str) -> str:
    r"""Return the timestamp of the last git ref."""
    return subprocess.run(
        [
            shlex.quote(git_executable),
            'log',
            '-1',
            '--pretty=%ct'
        ], check=True, capture_output=True).stdout.decode('UTF-8').strip()


##########
# Python #
##########
def build_dist(python_executable: str, git_executable: str):
    r"""Build the Python package in a reproducable way.

        Remove all dev, pre-releases, etc information from the package name.
        Use a static timestamp.
        See
        https://github.com/pypa/build/issues/328#issuecomment-877028239
    """
    subprocess.run(
        [
            shlex.quote(python_executable),
            '-m',
            'build',
            '--sdist',
            '--wheel',
            '-C--global-option=egg_info',
            '-C--global-option=--no-date',
            '-C--global-option=--tag-build=',
            '.'
        ], check=True, env=dict(os.environ, SOURCE_DATE_EPOCH=git_get_repository_timestamp(git_executable)))


def upload_dist(twine_executable: str, pypi_url: str, pypi_username: str, pypi_password: str):
    r"""Push the compiled package to a remote PyPI server."""
    subprocess.run(
        [
            shlex.quote(twine_executable),
            'upload',
            '--repository-url',
            pypi_url,
            '--non-interactive',
            '--skip-existing',
            'dist/*'
        ], check=True, env=dict(os.environ, TWINE_PASSWORD=pypi_password, TWINE_USERNAME=pypi_username))


def skip_objects(ignore_objects: dict, software_name: str) -> tuple:
    r"""Determine whether to skip repositories and/or tags."""
    skip_repository = False
    skip_tags = False
    if software_name in ignore_objects:
        if len(ignore_objects[software_name]) == 0:
            skip_repository = True
            skip_tags = False
        else:
            skip_tags = True

    return skip_repository, skip_tags


def set_base_directory_override(base_directory_override: dict, directory: str, software_name: str) -> pathlib.Path:
    r"""Change to the appropriate directory.

    :returns: directory, the path of the directory where the setup files
        are present.

    ..note: Values are reported in the remote configuration
    """
    old_directory = directory
    if software_name in base_directory_override:
        directory = pathlib.Path(directory, base_directory_override[software_name])
        # Check if inner_directory exists.
        # inner_directory usually is equal to absolute_directory
        if not directory.is_dir():
            # Fallback.
            directory = pathlib.Path(old_directory)
    else:
        directory = pathlib.Path(directory)

    return directory


def build_package_pre_post_commands(command_block: dict):
    for block in command_block:
        cmd = list()
        for c in command_block[block]:
            cmd.append(shlex.quote(c))
        try:
            subprocess.run(cmd, check=True)
        except subprocess.CalledProcessError as e:
            print(e)


def build_package(
    git_executable,
    python_executable,
    rm_executable: str,
    twine_executable,
    pypi_url,
    pypi_user,
    pypi_password,
    cache: dict,
    directory: str,
    software_name: str,
    reference: str,
    successful_tags: list,
    base_directory_override: dict,
    submodule_mark_failed_as_successful: bool,
    command_block_pre: dict,
    command_block_post: dict,
):
    r"""Checkout, compile and push.

    This function processes repositories without tags.
    In this case they are marked as successful but are not added to the cache.
    """
    directory_relative_path = directory.stem
    # Cleanup.
    git_remove_untracked_files(git_executable)

    print_information(directory_relative_path, reference, 'processing')

    # Checkout repository with tags.
    if reference != str():
        fpyutils.shell.execute_command_live_output(
            shlex.quote(git_executable)
            + ' checkout '
            + reference
        )

    # Decide whether to change directory.
    inner_directory = set_base_directory_override(base_directory_override, directory, software_name)
    subdirectory_absolute_path = str(inner_directory)
    with pushd(subdirectory_absolute_path):
        fpyutils.shell.execute_command_live_output(shlex.quote(rm_executable) + ' -rf build dist')
        try:
            build_package_pre_post_commands(command_block_pre)
            build_dist(python_executable, git_executable)
            build_package_pre_post_commands(command_block_post)

            upload_dist(twine_executable, pypi_url, pypi_user, pypi_password)

            # Register success in cache yaml file.
            if reference != str() and successful_tags != ['']:
                successful_tags.append(reference)
                cache[software_name].append(reference)

        except subprocess.CalledProcessError:
            print_information(directory.stem, reference, 'error')

            if submodule_mark_failed_as_successful:
                # Do not add an empty git reference to the cache.
                if reference != str():
                    successful_tags.append(reference)
                    cache[software_name].append(reference)

        git_remove_untracked_files(git_executable)


def read_remote_configuration(repository_path: str) -> dict:
    """Retrieve the configuration of the remote repository."""
    remote_configuration_file = pathlib.Path(repository_path, 'configuration.yaml')
    remote_config = dict()
    if remote_configuration_file.is_file():
        remote_config = yaml.load(open(remote_configuration_file, 'r'), Loader=yaml.SafeLoader)
        if (not check_configuration_structure_base_directory_override(remote_config)
           or not check_configuration_structure_checkout(remote_config)
           or not check_configuration_structure_build(remote_config)):
            raise InvalidConfiguration

    return remote_config


def process(
    cache: dict,
    cache_file: str,
    ignore_objects: dict,
    submodules_ignored_are_successuful,
    notify: dict,
    git_executable: str,
    python_executable: str,
    rm_executable: str,
    twine_executable: str,
    pypi_url: str,
    pypi_user: str,
    pypi_password: str,
    repository_path: str,
    repository_remote: str,
    repository_default_branch: str,
    submodule_mark_failed_as_successful,
):
    # Define and register the signals.
    def signal_handler(*args):
        print('\n==========================')
        print('signal received. writing ' + str(len(cache)) + ' repository elements to cache before exit')
        print('==========================\n')
        write_cache(cache, cache_file)
        sys.exit(0)
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    git_get_updates(git_executable, repository_path, repository_remote, repository_default_branch)

    remote_config = read_remote_configuration(repository_path)

    total_tags = 0
    total_successful_tags = 0
    # Go to the submodules subdirectory.
    repository_path = pathlib.Path(repository_path, 'submodules')
    for directory in pathlib.Path(repository_path).iterdir():

        successful_tags = list()
        # The software name is used in the cache as key.
        software_name = pathlib.Path(directory).stem
        skip_repository, skip_tags = skip_objects(ignore_objects, software_name)
        submodule_absolute_path = str(pathlib.Path(repository_path, directory))

        # Create a new cache slot.
        if (software_name not in cache
           and directory.is_dir()
           and software_name != '.git'):
            cache[software_name] = list()

        # Mark ignored repositories as successful if the setting is enabled.
        # Save in cache.
        if (skip_repository
           and submodules_ignored_are_successuful
           and directory.is_dir()
           and software_name != '.git'):
            with pushd(submodule_absolute_path):
                tags = git_get_tags(git_executable)
                # Bulk append: all tags are successful.
                successful_tags += tags
                cache[software_name] = successful_tags
        # Process the repository normally.
        elif (not skip_repository
              and directory.is_dir()
              and software_name != '.git'):
            with pushd(submodule_absolute_path):
                # Cleanup previous runs.
                git_remove_untracked_files(git_executable)

                # Get all git tags and iterate.
                tags = git_get_tags(git_executable)
                total_tags_original = len(tags)

                # Filter out tags that are in the ignore list.
                tags = git_filter_ignore_tags(tags, ignore_objects, skip_tags, software_name)

                # The 'checkout' section in the remote configuration rewrites all tags.
                if software_name in remote_config['submodules']['checkout']:
                    tags = remote_config['submodules']['checkout'][software_name]

                total_tags += len(tags)

                # Filter tags not already processed.
                tags = git_filter_processed_tags(tags, cache, software_name, successful_tags)

                # Get pre-post build commands.
                if software_name in remote_config['submodules']['build']['pre_commands']:
                    pre_commands = remote_config['submodules']['build']['pre_commands'][software_name]
                else:
                    pre_commands = dict()
                if software_name in remote_config['submodules']['build']['post_commands']:
                    post_commands = remote_config['submodules']['build']['post_commands'][software_name]
                else:
                    post_commands = dict()

                # Build the Python package.
                if total_tags_original == 0:
                    build_package(
                        git_executable,
                        python_executable,
                        rm_executable,
                        twine_executable,
                        pypi_url,
                        pypi_user,
                        pypi_password,
                        cache,
                        directory,
                        software_name,
                        str(),
                        successful_tags,
                        remote_config['submodules']['base_directory_override'],
                        submodule_mark_failed_as_successful,
                        pre_commands,
                        post_commands,
                    )
                else:
                    for t in tags:
                        build_package(
                            git_executable,
                            python_executable,
                            rm_executable,
                            twine_executable,
                            pypi_url,
                            pypi_user,
                            pypi_password,
                            cache,
                            directory,
                            software_name,
                            t,
                            successful_tags,
                            remote_config['submodules']['base_directory_override'],
                            submodule_mark_failed_as_successful,
                            pre_commands,
                            post_commands,
                        )
        total_successful_tags += len(successful_tags)

    write_cache(cache, cache_file)

    message = build_message(total_tags, total_successful_tags)
    print(message)
    send_notification(message, notify)


if __name__ == '__main__':
    def main():
        configuration_file = shlex.quote(sys.argv[1])
        config = yaml.load(open(configuration_file, 'r'), Loader=yaml.SafeLoader)
        if not check_configuration_structure(config):
            raise InvalidConfiguration

        dirs = AppDirs('build_pypi_packages')
        # Read the cache file.
        if config['files']['cache']['clear']:
            shutil.rmtree(dirs.user_cache_dir, ignore_errors=True)
        pathlib.Path(dirs.user_cache_dir).mkdir(mode=0o700, exist_ok=True, parents=True)
        cache_file = str(pathlib.Path(dirs.user_cache_dir, config['files']['cache']['file']))
        cache = read_cache_file(cache_file)

        process(
            cache,
            cache_file,
            config['submodules']['ignore'],
            config['submodules']['ignored_are_successuful'],
            config['notify'],
            config['files']['executables']['git'],
            config['files']['executables']['python'],
            config['files']['executables']['rm'],
            config['files']['executables']['twine'],
            config['pypi']['url'],
            config['pypi']['username'],
            config['pypi']['password'],
            config['repository']['path'],
            config['repository']['remote'],
            config['repository']['default branch'],
            config['submodules']['mark_failed_as_successful']
        )

    main()

automated-tasks
===============

|buymeacoffee|

.. |buymeacoffee| image:: assets/buy_me_a_coffee.svg
                   :alt: Buy me a coffee
                   :target: https://buymeacoff.ee/frnmst

A collection of `free software <https://www.gnu.org/philosophy/free-sw.html>`_
scripts that I currently use on my computer systems.

.. image:: assets/logo.png
     :target: https://docs.franco.net.eu.org/automated-tasks/scripts.html
     :align: center

Issue tracker
-------------

https://software.franco.net.eu.org/frnmst/automated-tasks/issues

You can use your GitHub account to interact.
Have a look `here <https://software.franco.net.eu.org/frnmst/software.franco.net.eu.org/src/branch/master/privacy_policy.md#accounts>`_

Documentation
-------------

https://docs.franco.net.eu.org/automated-tasks/

.. image:: assets/read_this.jpg
     :target: https://docs.franco.net.eu.org/automated-tasks/
     :align: center

Assets' credits
---------------

- the *Hey! Read this.* image reported here is released under the
  `CC BY 2.0 license <https://creativecommons.org/licenses/by/2.0/>`_ by
  Michael Downey. See the
  `original source <https://www.flickr.com/photos/michaeljdowney/2174625842>`_.

- the logo is composed of three pictures:

  - https://commons.wikimedia.org/wiki/File:Systemd-logo.svg

    - `Creative Commons Attribution 3.0 Unported license <https://creativecommons.org/licenses/by/3.0/deed.en>`_
    - `original file <https://upload.wikimedia.org/wikipedia/commons/3/33/Systemd-logo.svg>`_
    - author: ``Tobias Bernard``

  - https://en.wikipedia.org/wiki/File:Gnome-utilities-terminal.svg

    - `Creative Commons Attribution-Share Alike 3.0 Unported license <https://creativecommons.org/licenses/by-sa/3.0/deed.en>`_
    - `original file <https://upload.wikimedia.org/wikipedia/commons/d/da/Gnome-utilities-terminal.svg>`_
    - author: ``GNOME Project``

  - https://commons.wikimedia.org/wiki/File:Big-bot-icon.svg

    - `Creative Commons Attribution 3.0 Unported license <https://creativecommons.org/licenses/by/3.0/deed.en>`_
    - `original file <https://upload.wikimedia.org/wikipedia/commons/d/d8/Big-bot-icon.svg>`_
    - author: ``Robot by Creaticca Creative Agency from the Noun Project``

License
-------

Unless otherwise noted:

Copyright (C) 2019-2022 Franco Masotti (franco \D\o\T masotti {-A-T-} tutanota \D\o\T com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Changelog and trusted source
----------------------------

You can check the authenticity of new releases using my public key.

Changelogs, instructions, sources and keys can be found at `blog.franco.net.eu.org/software <https://blog.franco.net.eu.org/software/>`_.

Crypto donations
----------------

- Bitcoin: bc1qnkflazapw3hjupawj0lm39dh9xt88s7zal5mwu
- Monero: 84KHWDTd9hbPyGwikk33Qp5GW7o7zRwPb8kJ6u93zs4sNMpDSnM5ZTWVnUp2cudRYNT6rNqctnMQ9NbUewbj7MzCBUcrQEY
- Dogecoin: DMB5h2GhHiTNW7EcmDnqkYpKs6Da2wK3zP
- Vertcoin: vtc1qd8n3jvkd2vwrr6cpejkd9wavp4ld6xfu9hkhh0

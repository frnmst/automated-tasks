Deprecated Scripts
==================

Here is a list of obsolete scripts and their replacements, if available.

Scripts might be present in this list just because they are unmantained.

================================     =======================================    ========================================
Old script name                      Current script, program name or comment    Last available in commit
================================     =======================================    ========================================
``android_phone_backup.sh``          Nextcloud                                  a25d0ecd3e75993bab9c3ed7a153608130a0d129
``archive_documents_simple.sh``      ``borgmatic_hooks.py``                     8b45cecb149595a73628a1afff15f8161e22d467
``simple_backup.sh``                 ``borgmatic_hooks.py``                     8b70a5ebfa42f03f860621163366b02884bcf0cb
``vdirsyncer``                       Nextcloud                                  fec5652df11e64a263aee9ad2d219b926604a14a
``random_wallpaper.sh``              not using anymore                          38405e8d12b6d6876a160432b2097a771c49b546
``xfs_defrag.py``                    not using XFS anymore                      38405e8d12b6d6876a160432b2097a771c49b546
================================     =======================================    ========================================
